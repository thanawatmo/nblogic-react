import React from "react";
import {
    Button,
    Form,
    Grid,
    Header,
    Image,
    Message,
    Segment
} from "semantic-ui-react";
import { Redirect } from "react-router-dom";

// const LoginForm = () => (
//   <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
//     <Grid.Column style={{ maxWidth: 450 }}>
//       <Header as='h2' color='orange' textAlign='center'>
//         <Image src='/nblogo.png' style={{ marginBottom: 5 }}/> Log-in to your account
//       </Header>
//       <Form size='large'>
//         <Segment stacked>
//           <Form.Input fluid icon='user' iconPosition='left' placeholder='E-mail address' />
//           <Form.Input
//             fluid
//             icon='lock'
//             iconPosition='left'
//             placeholder='Password'
//             type='password'
//           />

//           <Button color='orange' fluid size='large'>
//             Login
//           </Button>
//         </Segment>
//       </Form>
//       <Message>
//         New to us? <a href='#'>Sign Up</a>
//       </Message>
//     </Grid.Column>
//   </Grid>
// )

class Login extends React.Component {
    state = {
        redirect: false
    };

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect push to="/about" />;
        }
    };

    render() {
        return (
            
            <Grid
                textAlign="center"
                style={{ height: "100vh" }}
                verticalAlign="middle"
            >
                {this.renderRedirect()}
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as="h2" color="orange" textAlign="center">
                        <Image src="/nblogo.png" style={{ marginBottom: 5 }} />{" "}
                        Log-in to your account
                    </Header>
                    <Form size="large">
                        <Segment stacked>
                            <Form.Input
                                fluid
                                icon="user"
                                iconPosition="left"
                                placeholder="E-mail address"
                            />
                            <Form.Input
                                fluid
                                icon="lock"
                                iconPosition="left"
                                placeholder="Password"
                                type="password"
                            />
                            
                            <Button
                                color="orange"
                                fluid
                                size="large"
                                onClick={this.setRedirect}
                            >
                                Login
                            </Button>
                        </Segment>
                    </Form>
                    <Message>
                        New to us? <a href="#">Sign Up</a>
                    </Message>
                </Grid.Column>
            </Grid>
        );
    }
}

export default Login;
