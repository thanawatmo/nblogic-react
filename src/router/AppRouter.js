import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { List } from "semantic-ui-react";
import { Users, Home, About} from '../pages'


function AppRouter() {
    return (
        <Router>
            <div>
                {/* <List>
                    <List.Item>
                        <Link to="/">Home</Link>
                    </List.Item>
                    <List.Item>
                        <Link to="/about/">About</Link>
                    </List.Item>
                    <List.Item>
                        <Link to="/users/">Users</Link>
                    </List.Item>
                </List> */}

                <Route path="/" exact component={Home} />
                <Route path="/about/" component={About} />
                <Route path="/users/" component={Users} />
            </div>
        </Router>
    );
}

export default AppRouter;
